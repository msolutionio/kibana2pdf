# author: Pierre Beaumadier (pierre.beaumadier@gmail.com)

FROM ubuntu:14.04

ENV DEBIAN_FRONTEND noninteractive

# required packages
RUN apt-get update
RUN apt-get install -y python3-flask python3-yaml python3-reportlab
RUN apt-get install -y phantomjs
RUN apt-get install -y git ssh

ADD . /home/kibana2pdf/

# flask port
EXPOSE 5000

# run application
CMD python3 /home/kibana2pdf/generate-pdf.py
