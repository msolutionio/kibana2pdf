#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# author: Pierre Beaumadier (pierre.beaumadier@gmail.com)

import sys, os

if sys.version_info.major < 3:
	exit('Python 3 is required')

PHANTOMJSBIN = '/usr/bin/phantomjs'
JSFILE = 'get-screenshot.js'

if os.path.isfile(PHANTOMJSBIN) is not True:
	exit('PhantomJS is required')

from datetime import datetime

today = datetime.today()
report_filename = './pdfs/monitoring_report_' + today.strftime('%m%d%Y-%H%M%S-%f') + '.pdf'

def create_pdf_report(template):
	
	import yaml
	
	data = yaml.load(open(template))
	title = data['title']
	urls_list = data['urls']

	# create pdf file
	from reportlab.pdfgen import canvas
	from reportlab.pdfbase import pdfmetrics  

	doc = canvas.Canvas(report_filename)

	# cover page
	# CBS logo
	doc.drawImage('./images/CBS-logo.jpg', 150, 600, 300, 150);
	doc.setFontSize(24);
	# doc title
	doc.drawCentredString(300, 500, title)
	# today's date display
	doc.setFontSize(16);
	doc.drawCentredString(300, 400, 'Date : ' + today.strftime('%m/%d/%Y'))
	# Msolution logo
	doc.drawImage('./images/msolution-logo.jpg', 200, 100, 160, 140);
	doc.drawCentredString(280, 50, str(doc.getPageNumber()))

	if data['toc'] is True:
		doc.showPage()
		doc.setFontSize(16);
		doc.drawCentredString(300, 800, 'Table of contents')
		doc.setFontSize(12);
		for i, url in enumerate(urls_list):
			doc.drawString(40, 780 - (i + 1) * 40, url[47:].replace('-', ' ').capitalize())
			doc.drawString(520, 780 - (i + 1) * 40, 'page ' + str(i + 3))

	# get all screenshots and put them in a page one by one
	import subprocess

	for i, url in enumerate(urls_list):
		subprocess.call([PHANTOMJSBIN, JSFILE , url, './images/screenshot' + str(i) + '.jpg'])
		doc.showPage()
		doc.setFontSize(16);
		doc.drawCentredString(300, 800, url[47:].replace('-', ' ').capitalize())
		doc.setFontSize(12);
		doc.drawImage('./images/screenshot' + str(i) + '.jpg', 100, 300, 400, 320);
		doc.drawCentredString(300, 50, str(doc.getPageNumber()))

	# write doc on disk
	doc.save()
	return 0

if len(sys.argv) > 1 and (sys.argv[1] == '--template' or sys.argv[1] == '-t'):
	create_pdf_report(sys.argv[2])
else:
	from flask import Flask, request, render_template, flash, send_file

	app = Flask(__name__)
	app.secret_key = 'ludojetaime'

	@app.route('/', methods=['GET', 'POST'])
	def index_form():
		if request.method == 'POST':
			create_pdf_report('./templates/' + request.form['tmpl'] + request.form['trng'] + '.yml')
			flash(u'PDF generated')
			return send_file(report_filename, as_attachment=True)
		return render_template('form.html')

	if __name__ == '__main__':
		app.run(host='0.0.0.0', debug=False) # debug to set to False in production, remove host parameter for local use

# end
