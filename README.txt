============
 kibana2pdf
============

Small program to generate a report based on several Kibana urls.

Requires :
- Python 3.x, with flask, yaml and reportlab modules
- Phantomjs (http://phantomjs.org/)
