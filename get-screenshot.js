// author : Pierre Beaumadier (pierre.beaumadier@gmail.com)

var page = require('webpage').create();
var system = require('system');
var address = system.args[1];
var output = system.args[2];
var waitTime = 6 * 1000; // page can take time to load

page.viewportSize = { width: 800, height: 800 };
page.open(address, function (status) {
	if (status !== 'success') {
		console.log('Unable to access the address');
		phantom.exit(1);
	} else {
		window.setTimeout(function () {
			page.clipRect = { top: 160, left: 0, width: 800, height: 640 }; // cut the header
			page.render(output);
			phantom.exit();
		}, waitTime);
	}
});
